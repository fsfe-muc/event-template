= Checkliste für Events =

Hallo! Die tollste Veranstaltung bringt uns nichts, wenn niemand kommt.
Deshalb haben wir diese Checkliste zusammengestellt, um unsere Events möglichst
gut zu planen, zu kommunizieren und abzuwickeln.

Der komplette Inhalt dieses Repositorys ist im VimWiki-Format (momentan gibt es
nur diese README-Datei, das sollte sich aber evtl. ändern.) Es sollte sich
einfach klonen und als Basis für konkrete Events nutzen lassen.


== Im Vorfeld ==

- [ ] Falls nötig: ein Planungs-Treffen auf unserer Mailingliste ausrufen (auf
      dem kann auch der Rest dieser Checkliste abgearbeitet werden)


== Ankündigen ==

- [ ] Einen Einladungstext schreiben
- [ ] Die Veranstaltung auf unserem Wiki https://wiki.fsfe.org/LocalGroups/Muenchen/ ankündigen
- [ ] Die Veransaltung auf https://fsfe.org ankündigen
    - [ ] Den Einladungstext ins Englische übersetzen
    - [ ] Das Event _mit englischem Einladungstext_ auf https://fsfe.org/community/tools/eventregistration.en.html registrieren
- [ ] u.U. die Seite auf https://cpu.ccc.de ankündigen
- [ ] Die Veranstaltung auf der Website des Verantaltungsortes Ankündigen
    - [ ] Falls die Veranstaltung im WikiMUC stattfindet: …
        - [ ] Die Veranstaltung in den Terminkalender des WikiMUC eintragen.
              Dies sorgt dafür, dass sie auch auf den Terminkalender-Flyern des
              WikiMUCs landet.
        - [ ] Den Einladungstext auf die WikiMUC-Seite des Termins stellen
    - [ ] Falls die Veranstaltung im Rahmen des OpenSource-Treffens stattfindet: …
- [ ] Die Veranstaltung auf allen relevanten Mailinglisten Ankündigen:
    - [ ] mailto:fsfe-muc.lists.fsfe.de  bzw. mailto:weisswurst.lists.fsfe.de
    - [ ] mailto:osstreffen-discuss
    - [ ] mailto:fsfe-de.lists.fsfe.de
- [ ] u.U einen eigenen Flyer designen.


== Nacharbeiten ==

- [ ] …
